package ru.kuzin.tm.api.service;

import ru.kuzin.tm.api.repository.IProjectRepository;
import ru.kuzin.tm.enumerated.Sort;
import ru.kuzin.tm.enumerated.Status;
import ru.kuzin.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IProjectRepository {

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

    List<Project> findAll(Comparator<Project> comparator);

    List<Project> findAll(Sort sort);

}